package cat.epiaedu.pmdm.jonatan.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.epiaedu.pmdm.jonatan.Joctest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title="Joc test Jonatan";
		new LwjglApplication(new Joctest(), config);
	}
}
