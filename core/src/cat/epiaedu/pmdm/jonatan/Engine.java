package cat.epiaedu.pmdm.jonatan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by jonatan.escalera on 08/02/2017.
 */

public class Engine {

    //sounds variables
    public boolean on;

    private long idle, run;
    private float pitch;
    private long song;
    private Sound idleMotor,runMotor ;
    private boolean motorOnOff,acelerating;

    float tick=0.01f;
    float tickTime;

   public Engine(){
       //Creamos sounds
       idleMotor= Gdx.audio.newSound(Gdx.files.internal("sounds/engine-idle.wav"));
       runMotor=Gdx.audio.newSound(Gdx.files.internal("sounds/engine-running.wav"));

        acelerating=false;
       //run=runMotor.play();
       //pitch=0;
    }

    public void startEngine()
    {
        //idle=idleMotor.play();
        on=true;
        idle=idleMotor.loop();
    }
    public void offEngine()
    {
        on=false;
        idleMotor.stop(idle);
        runMotor.stop(run);
    }
    public void acelerating(float deltaTime)
    {
        if(!acelerating) startAcelerating();
        tickTime+=deltaTime-tickTime;

        Gdx.app.log("TickTime: ",String.valueOf(tickTime));

        if(pitch<=2){
            pitch+=(tickTime/1000);
        }
        runMotor.setPitch(run, pitch);
        Gdx.app.log("Pitch ",String.valueOf(pitch));

    }

    private void startAcelerating()
    {
        idleMotor.stop(idle);
        run=runMotor.play();

        tickTime=0;
        pitch=0.5f;
        acelerating=true;
    }

    public void startDesacelerating(float deltaTime)
    {
        tickTime+=deltaTime-tickTime;

        if(pitch>0) {
            pitch -= (tickTime / 1000);
            runMotor.setPitch(run, pitch);
        }
        else
        {
            runMotor.stop(run);
            //idleMotor.dispose();
            runMotor.dispose();
        }


    }
}
