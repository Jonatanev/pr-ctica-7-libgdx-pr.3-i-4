package cat.epiaedu.pmdm.jonatan;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jonatan.escalera on 25/01/2017.
 */
public class helloWorld {
    private SpriteBatch batch;

    private BitmapFont font;





    public void create() {

        batch = new SpriteBatch();

        font = new BitmapFont();

        font.setColor(Color.WHITE);

    }



    public void render() {

        Gdx.gl.glClearColor(1, 1, 1, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);



        batch.begin();

        font.draw(batch, "HOLA MON", 300, 0);

        batch.end();

    }



    public void dispose() {

        batch.dispose();

        font.dispose();

    }
}
