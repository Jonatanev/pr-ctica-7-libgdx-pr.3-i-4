package cat.epiaedu.pmdm.jonatan;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.sql.Time;

public class Joctest implements ApplicationListener,InputProcessor {
	SpriteBatch batch;
	TextureAtlas txtAtlas;
	Animation animation;
	Texture img, texture;
	Pixmap pixmap;
	Sprite sprite;
	Rectangle bird;
	private float elapsedTime=0;
	private OrthographicCamera camera;

	float tick=0.5f;
	float tickTime;

	//Motor
	Engine motor;
	boolean onMotor;

	@Override
	public void create () {
		batch = new SpriteBatch();
		txtAtlas=new TextureAtlas(Gdx.files.internal("atlas/pack.atlas"));
		animation=new Animation(1/15f, txtAtlas.getRegions());

		//Creamos Pajaro
		bird=new Rectangle();
		bird.x=800/2 -180/2;
		bird.y=20;
		bird.width=180;
		bird.height=180;

		//Creamos motor
		motor=new Engine();
		onMotor=false;
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		///UPDATE
		// create the camera and the SpriteBatch
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);

		//Inputs///
		if(Gdx.input.justTouched()) {
			Vector3 touchP=new Vector3();
			touchP.set(Gdx.input.getX(),Gdx.input.getY(),0);
			camera.unproject(touchP);
			bird.x = touchP.x - 180 / 2;
			bird.y = touchP.y - 180 / 2; //

		}
		//Inputs sounds
		if(Gdx.input.isKeyJustPressed(Input.Keys.CONTROL_LEFT))
		{
			if(!onMotor){
				motor.startEngine();
				onMotor=true;
				Gdx.app.log("ENgine","Motor ON");
			}
			else {
				motor.offEngine();
				onMotor=false;
				Gdx.app.log("ENgine","Motor OFF");
			}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
		{
			if(onMotor)	motor.acelerating(elapsedTime);

		}
		else{
			//desacelerar o apagar motor
			//motor.startDesacelerating(elapsedTime);
		}

		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) bird.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) bird.x += 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) bird.y -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.UP)) bird.y += 200 * Gdx.graphics.getDeltaTime();
		// chocar pared
		if(bird.x < 0) bird.x = 0;
		if(bird.x > 800 - 180) bird.x = 800 - 180;


		//DRAW/////
		batch.begin();

		elapsedTime+=Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime,true), bird.x, bird.y);

		batch.end();
	}

	@Override
	public void pause() {

		Gdx.app.log("text","Joc en pause");
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose () {
		batch.dispose();
		//img.dispose();
		//idleMotor.dispose();
		//runMotor.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
